package org.forrest.websocket;

import static org.forgerock.json.JsonValue.*;

import java.io.IOException;

import javax.websocket.CloseReason;
import javax.websocket.EncodeException;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.forgerock.json.JsonValue;

/**
 * @since 1.0
 */
@ServerEndpoint(value = "/datatransfer/{uid}", encoders = JsonValueEncoder.class, decoders = JsonValueDecoder.class)
public final class DataTransferEndpoint {

    private static final Logger logger = LogManager.getLogger(DataTransferEndpoint.class);

    @OnMessage
    public void onMessage(Session session, JsonValue message, @PathParam("uid") String uid) {
        try {
            session.getBasicRemote().sendObject(json(object(field("uid", uid))));
        } catch (IOException | EncodeException e) {
            logger.error("Send failed", e);
        }
    }

    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        logger.debug("onOpen called");
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) {
        logger.debug("onClose called");
    }

    @OnError
    public void onError(Session session, Throwable error) {
        logger.debug("onError called", error);
    }

}
